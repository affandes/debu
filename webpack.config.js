const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: "app.js"
    },
    module: {
        rules: [
            {
                test: /\.(s*)css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(jpg|png|gif|ico|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[hash:8].[ext]',
                        },
                    },
                ]
            },
            {
                test: /\.mp3$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[hash:8].[ext]',
                        },
                    },
                ]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery'",
            "window.$": "jquery"
        })
    ]
};
/**
 * Efek Thanos
 */

import $ from 'jquery'
import html2canvas from 'html2canvas'
import 'jquery-ui/ui/core'
import 'jquery-ui/ui/effect'
import './app.scss'

import IronMan from './ironman.png'
import SnapSound from './thanos_snap_sound.mp3'
import DustSound from './thanos_dust_1.mp3'

var elmWidth = 600;
var elmHeight = 240;
var debuWidth = 40;
var debuHeight = 40;

$(document).ready(function (el) {

    // Set image
    const pic = new Image();
    pic.src = IronMan;
    $('#image').html(pic);

    // Konfig
    var num = 32;
    var listImageData = [];

    // Snap!
    $('#snap').click(function (o) {

        html2canvas(document.getElementsByClassName('container')[0]).then(canvas => {

            // Play sound
            var sound = new Audio(SnapSound);
            sound.play();

            setTimeout(() => {
                var dust1 = new Audio(DustSound);
                dust1.play();
            },2200);

            // Get ImageData
            let context = canvas.getContext('2d');
            let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
            let rgbArray = imageData.data;

            // Create empty canvas
            for (let i = 0; i < num; i++) {
                let emptyRgbArray = new Uint8ClampedArray(rgbArray.length);
                listImageData.push(emptyRgbArray);
            }

            // Distribute rgb to canvas
            let rgbLength = rgbArray.length;
            for (let i = 0; i < rgbLength; i+=4) {
                let maxRandom = (i/rgbLength) * num;
                let index = Math.floor(Math.random() * maxRandom);
                let choosenCanvas = listImageData[index];
                // Distributing
                choosenCanvas[i] = rgbArray[i]; // Red
                choosenCanvas[i+1] = rgbArray[i+1]; // Green
                choosenCanvas[i+2] = rgbArray[i+2]; // Blue
                choosenCanvas[i+3] = rgbArray[i+3]; // Alpha
            }

            // Create element canvas
            for (let i = 0; i < num; i++) {
                let newCanvas = document.createElement('canvas');
                newCanvas.width = canvas.width;
                newCanvas.height = canvas.height;
                let newContext = newCanvas.getContext('2d');
                newContext.putImageData(new ImageData(listImageData[i], canvas.width, canvas.height), 0, 0);
                newCanvas.classList.add('debu');
                document.getElementsByClassName('container')[0].appendChild(newCanvas);
            }

            // Fading Out Original item
            $('.container').children().not('.debu').fadeOut(3500);

            // Set Animasi
            setTimeout(runEffect, 1200);
        })

    });

});

function runEffect() {
    var delay = 75;
    var duration = 2000;

    // Efek
    $('.debu').each(function (i) {
        // Debu hilang
        $(this).delay(delay * i).fadeOut(
            duration + (110 * i),
            'easeInQuad',
            () => {
                $(this).remove();
            }
        );
        // Efek debu
        setTimeout(() => {
            var tx = 0,ty = 0, td = 0;
            $(this).animate(
                {
                    x: 200,
                    y:-200,
                    deg:(Math.random()*30) - 15
                },
                {
                    duration: duration + (110 * i),
                    easing: 'easeInQuad',
                    queue: false,
                    step: function(now, fx) {
                        if (fx.prop == "x")
                            tx = now;
                        else if (fx.prop == "y")
                            ty = now;
                        else if (fx.prop == "deg")
                            td = now;
                        $(this).css({
                            transform: 'rotate(' + td + 'deg)' + 'translate(' + tx + 'px,'+ ty +'px)',

                        });
                    },
                    complete: function () {

                    }
                }
            )
        }, delay * i);

    });
}